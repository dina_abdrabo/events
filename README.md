* > 1-Install Laragon and lunch it
* > 2-Create Database with name "events" and import the events.sql in your phpmyadmin database
* > 3-pull the project
* > 4-add the project folder in www directory
* > 5- open in the browser localhost/events/wp-admin
* > 6-Admin username : dina  & password : 123456789
* > 7-start test with postman for testing APIs





**APIS Endpoints**



1. User  

user registeration


* Add user  
POST http://localhost/events/wp-json/wp/v2/users
body attributes: 
username: "name"
password:"your selected password"
email:"unique email"
roles : author/admin



2. Meetings 

* Add event 
* POST http://localhost/events/wp-json/wp/v2/meeting
* body attributes: 
* title : "xyz"
* author : user_id
* status : "publish"
* time : ""
* location : ""
* user_one : "name/email"
* user_two : "name/email"
* user_third : "name/email"
* user_fourth : "name/email"
* 
* Edit Meeting
* POST http://localhost/events/wp-json/wp/v2/meeting/{id}
* body attributes for selected one that you want to change: 
* title : "xyz"
* author : user_id
* status : "publish"
* time : ""
* location : ""
* user_one : "name/email"
* user_two : "name/email"
* user_third : "name/email"
* user_fourth : "name/email"
* 
* 
* 
* Delete Meeting
* DELETE http://localhost/events/wp-json/wp/v2/meeting/{id}
* 



3. Calls 

* Add call 
* POST http://localhost/events/wp-json/wp/v2/call
* 
* body attributes: 
* 
* title : "xyz"
* author : user_id
* status : "publish"
* time : ""
* location : ""
* user_one : "name/email"
* user_two : "name/email"
* 
* Edit Call
* POST http://localhost/events/wp-json/wp/v2/call/{id}
* 
* body attributes for selected one that you want to change: 
* title : "xyz"
* author : user_id
* status : "publish"
* time : ""
* location : ""
* user_one : "name/email"
* user_two : "name/email"
* 
* 
* 
* Delete Call
* DELETE http://localhost/events/wp-json/wp/v2/call/{id}



